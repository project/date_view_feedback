date_view_feedback

README

Description
------------

This module shows how the view interpreted the dates you typed into the exposed
filters.  If your input was not understood, you will get a validation error.
If you typed in a date in non-canonical format, it will be shown to you in
canonical format when you see the search results.


Limitations
-----------

This module only understands the jscal widget.  It was designed on a custom
site that backed up the jscal widget with strtotime.  Normally, jscal only
accepts input in canonical format anyway, so the translation to canonical
format might not be that useful to most people.


Installation
------------

Unpack the tarball and install in your modules directory.  Enable it on the
modules admin page.  In order to install modules, though, you'll need to make
the directory you intend to install to writable by the web-server user.
